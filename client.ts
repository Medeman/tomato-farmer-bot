import { Client } from 'coward/mod.ts'
import { configTree } from './config/index.ts'

export const client = new Client(configTree.token)
