export interface CommandInfo {
  command: string
  args: string[]
}
