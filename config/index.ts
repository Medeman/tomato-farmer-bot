import { readFileStr } from 'std/fs/mod.ts'
import { ConfigTree } from './config-tree.interface.ts'
// const config: ConfigTree = JSON.parse(await readFileStr('./.config.json'))

class Config {
  private _tree?: ConfigTree

  private async _loadConfig(): Promise<void> {
    this._tree = JSON.parse(await readFileStr('./.config.json'))
  }

  public async getTree(): Promise<ConfigTree> {
    if (!this._tree) {
      await this._loadConfig()
    }

    return this._tree!
  }
}

export const config = new Config()

export const configTree = await config.getTree()
