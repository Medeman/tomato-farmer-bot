export interface ConfigTree {
  token: string,
  announcer: {
    enabled: boolean
    randomMessageChannelIDs: string[],
    interval: number,
    probability: number
  }
}
