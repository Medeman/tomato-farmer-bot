import { CommandParser } from './command-parser.ts'
import { Roll } from './commands/roll.ts'
import { Spin } from './commands/spin.ts'
import { config } from './config/index.ts'
import { client } from './client.ts'
import { AnnounceHandler } from './announce-handler.ts'
import { WikipediaAnnouncer } from './announcers/wikipedia-announcer.ts'

// import { WikipediaAnnouncer } from './wikipedia-announcer.ts'

const parser = new CommandParser()

parser.registerCommand(new Roll())
parser.registerCommand(new Spin())

parser.init()

const announceHandler = new AnnounceHandler()

announceHandler.registerAnnouncer(new WikipediaAnnouncer())

announceHandler.init()

client.connect()

console.log('Ready.')
