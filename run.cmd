if "%1"=="-r" (
deno run^
  --allow-net^
  --allow-read^
  --unstable^
  --importmap=import_map.json^
  --reload^
  index.ts
) else (
deno run^
  --allow-net^
  --allow-read^
  --unstable^
  --importmap=import_map.json^
  index.ts
)
