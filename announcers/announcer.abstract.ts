import { User } from 'coward/mod.ts'

export abstract class Announcer {
  public abstract readonly name: string

  public abstract async execute(user: User): Promise<string | void>
}
