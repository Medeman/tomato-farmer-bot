import { User } from 'coward/mod.ts'
import { Announcer } from './announcer.abstract.ts'

const WIKIPEDIA_RANDOM_API_URL = 'https://de.wikipedia.org/w/api.php?format=j' +
  'son&action=query&generator=random&grnnamespace=0&prop=info&inprop=url'

export class WikipediaAnnouncer implements Announcer {
  public name = 'wikipedia'

  private async _fetchWikipediaURL(): Promise<string> {
    const response = await fetch(WIKIPEDIA_RANDOM_API_URL)
    const parsedResponse = await response.json()

    const articleID = Object.keys(parsedResponse.query.pages)[0]

    return parsedResponse.query.pages[articleID].fullurl
  }

  public async execute(user: User): Promise<string | void> {
    let url: string

    try {
      url = await this._fetchWikipediaURL()
    } catch (ex) {
      console.warn(ex)
      return
    }

    return `Hey <@${user.id}>, das könnte dich interessieren: ${url}`
  }
}
