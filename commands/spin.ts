import { Message, Guild } from 'coward/mod.ts'
import { Command } from './command.abstract.ts'
import { ArgumentInfo } from './argument-info.interface.ts'
import { Arguments } from './arguments.interface.ts'
import { ArgumentType } from './argument-type.enum.ts'
import { getChannelUsers, randomFrom } from '../helpers.ts'

// interface RollArguments extends Arguments {
//   dice: number
//   amount: number
// }

export class Spin implements Command {
  public name = 'spin'

  public help = 'Dreht eine Flasche, die bei Stillstand auf einen User zeigt'

  public argInfos: ArgumentInfo[] = []

  public execute(message: Message): string {
    const userIDs = getChannelUsers(message.channel.id)
      .map(user => user.id)
    let matchedGuild: Guild | void

    return `Die Flasche zeigt auf <@${randomFrom<string>(userIDs)}>.`
  }
}
