import { ArgumentType } from './argument-type.enum.ts'

export interface ArgumentInfo {
  name: string
  type: ArgumentType,
  required: boolean,
  help: string,
  fallback: string | number | void
}
