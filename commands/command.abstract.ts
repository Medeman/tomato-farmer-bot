import { Message } from 'https://deno.land/x/coward@dev/mod.ts'
import { ArgumentInfo } from './argument-info.interface.ts'
import { Arguments } from './arguments.interface.ts'

export abstract class Command {
  public abstract readonly name: string
  public abstract readonly argInfos: ArgumentInfo[]
  public abstract readonly help: string

  public abstract execute(
    message: Message,
    args: Arguments
  ): string | void
}
