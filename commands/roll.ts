import { Message } from 'coward/mod.ts'
import { Command } from './command.abstract.ts'
import { ArgumentInfo } from './argument-info.interface.ts'
import { Arguments } from './arguments.interface.ts'
import { ArgumentType } from './argument-type.enum.ts'
import { randomNumber } from '../helpers.ts'

interface RollArguments extends Arguments {
  dice: number
  amount: number
}

export class Roll implements Command {
  public name = 'roll'

  public help = 'Einen oder mehrere Würfel werfen'

  public argInfos: ArgumentInfo[] = [
    {
      name: 'dice',
      type: ArgumentType.Integer,
      required: false,
      help: 'Art des Würfels (bspw. 6 für W6, 20 für W20)',
      fallback: 6
    },
    {
      name: 'amount',
      type: ArgumentType.Integer,
      required: false,
      help: 'Anzahl der Würfel (bspw. 1)',
      fallback: 1
    }
  ]

  public execute(
    message: Message,
    args: RollArguments
  ): string {
    let { dice, amount } = args

    if (dice > 1000) {
      dice = 1000
    } else if (dice < 2) {
      dice = 2
    }

    if (!amount || amount < 0) {
      amount = 1
    } else if (amount > 100) {
      amount = 100
    }

    const results: number[] = []

    for (let i = 0; i < amount; ++i) {
      results.push(randomNumber(1, dice))
    }

    return `Ergebnis von ${amount} W${dice} Würfen: ${results.join(', ')}`
  }
}
