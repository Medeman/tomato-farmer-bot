export interface Arguments {
  [key: string]: string | number | void
}
