import { User, Guild } from 'coward/mod.ts'
import { client } from './client.ts'

export function getChannelUsers(channelID: string): User[] {
  let matchedGuild: Guild | void

  for (const guild of client.guilds.values()) {
    if (guild.channels.has(channelID)) {
      matchedGuild = guild
      break
    }
  }

  if (typeof matchedGuild === 'undefined') {
    throw new Error('Message channel was not found in any guild.')
  }

  const users: User[] = []

  matchedGuild.members.forEach(member => {
    if (!member.user.bot) {
      users.push(member.user)
    }
  })

  return users
}

export function randomNumber(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export function randomFrom<T>(arr: T[]): T {
  if (!arr.length) {
    throw new Error('Empty array given.')
  }

  return arr[randomNumber(0, arr.length - 1)]
}

export function probability(percent: number): boolean {
  if (percent >= 1) {
    return true
  }

  if (percent <= 0) {
    return true
  }

  return percent > Math.random()
}
