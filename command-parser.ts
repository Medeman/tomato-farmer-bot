import { Message, User } from 'coward/mod.ts'
import { CommandInfo } from './command-info.interface.ts'
import { Command } from './commands/command.abstract.ts'
import { Arguments } from './commands/arguments.interface.ts'
import { ArgumentInfo } from './commands/argument-info.interface.ts'
import { ArgumentType } from './commands/argument-type.enum.ts'
import { client } from './client.ts'

const MATCH_COMMAND = /^!(\w+) ?(.*$)/ // $1 = command, $2 = args

// Args in "" and '' supported.
const SPLIT_ARGS = /(?:"([^"]+)")|(?:'([^']+)')|([^ ]+)/g

interface RegisteredCommands {
  [key: string]: Command
}

export class CommandParser {
  private _registeredCommands: RegisteredCommands = {}

  public init(): void {
    this._registerEvents()
  }

  public registerCommand(command: Command): void {
    this._registeredCommands[command.name] = command

    console.log(`Registered command '${command.name}'.`)
  }

  private _renderHelpText(): string {
    let text = ''

    for (const index of Object.keys(this._registeredCommands)) {
      const { name, help, argInfos } = this._registeredCommands[index]

      const argNames = argInfos.map(({ name, required, fallback }) => {
        let formattedName = name

        if (fallback) {
          formattedName += `=${fallback}`
        }

        if (!required) {
          formattedName = `[${formattedName}]`
        }

        return formattedName
      })

      let syntax = `!${name}`

      if (argNames.length) {
        syntax += ` ${argNames.join(' ')}`
      }

      text += `\`\n\n${syntax}\` - ${help}`

      for (const argInfo of argInfos) {
        text += `\nArgument \`${argInfo.name}\` - ${argInfo.help}`
      }
    }

    return text
  }

  private _messageIsCommand(content: string): boolean {
    return content.startsWith('!')
  }

  private _parseCommand(content: string): CommandInfo {
    const match = MATCH_COMMAND.exec(content)

    if (!match) {
      throw new Error('Invalid command received.')
    }

    const command = match[1].toLowerCase()
    const rawArgs = match[2]

    const commandInfo: CommandInfo = {
      command,
      args: []
    }

    if (!rawArgs.length) {
      return commandInfo
    }

    let m

    while ((m = SPLIT_ARGS.exec(rawArgs)) !== null) {
      if (m.index === SPLIT_ARGS.lastIndex) {
        ++SPLIT_ARGS.lastIndex
      }

      const arg = m[1] || m[2] || m[3]

      if (arg) {
        commandInfo.args.push(arg)
      }
    }

    return commandInfo
  }

  private _formatArgs(argInfos: ArgumentInfo[], rawArgs: string[]): Arguments {
    const args: Arguments = {}

    for (let i in argInfos) {
      const argInfo = argInfos[i]
      const raw = rawArgs[i]

      if (argInfo.required && !raw) {
        throw new Error(`Required argument '${argInfo.name}' not given.`)
      }

      let converted: string | number

      if (typeof raw === 'undefined') {
        if (typeof argInfo.fallback === 'undefined') {
          continue
        } else {
          converted = argInfo.fallback
        }
      } else if (typeof raw === 'undefined') {
        continue
      } else {
        switch (argInfo.type) {
          case ArgumentType.String:
            converted = raw
            break
          case ArgumentType.Integer:
            converted = parseInt(raw, 10)

            if (isNaN(converted)) {
              throw new Error(
                `Number conversion failed for argument '${argInfo.name}'.`
              )
            }
            break
          default:
            converted = raw
            break
        }
      }

      args[argInfo.name] = converted
    }

    return args
  }

  private _reply(message: Message, author: User, content: string): void {
    client.postMessage(message.channel.id, `<@${author.id}>: ${content}`)
  }

  private _onMessageCreated(message: Message): void {
    const { content, author, timestamp } = message

    if (message.author.bot) {
      return
    }

    if (!this._messageIsCommand(content)) {
      return
    }

    const { username, discriminator }  = author

    const fullUsername = `${username}#${discriminator}`

    let command: string
    let args: string[]

    try {
      const parsedCommand = this._parseCommand(content)

      command = parsedCommand.command
      args = parsedCommand.args
    } catch (ex) {
      console.warn(ex)
      return
    }

    console.log(
      '\n',
      'Timestamp:', timestamp, '\n',
      'Username:', fullUsername, '\n',
      'Command:', command, '\n',
      'Arguments:', args
    )

    if (command === 'help') {
      // Do some help related stuff...
      this._reply(message, author, this._renderHelpText())
      return
    } else if (!this._registeredCommands[command]) {
      this._reply(message, author, 'Befehl nicht gefunden :(')
      return
    }

    const { argInfos } = this._registeredCommands[command]

    let formattedArgs: Arguments

    try {
      formattedArgs = this._formatArgs(argInfos, args)
    } catch (ex) {
      console.warn(ex)
      this._reply(message, author, 'Ungültige Argumente!')
      return
    }

    let result: string | void

    // message.channel.id

    try {
      result = this._registeredCommands[command].execute(message, formattedArgs)
    } catch (ex) {
      console.warn(ex)
      this._reply(message, author, 'Befehlausführung fehlgeschlagen!')
      return
    }

    if (!result) {
      return
    }

    this._reply(message, author, result)
  }

  private _registerEvents(): void {
    client.evtMessageCreate.attach(({ message }) => {
      this._onMessageCreated(message)
    })
  }
}
