import { User } from 'coward/mod.ts'
import { client } from './client.ts'
import { configTree } from './config/index.ts'
import { probability, getChannelUsers, randomFrom } from './helpers.ts'
import { Announcer } from './announcers/announcer.abstract.ts'

interface RegisteredAnnouncers {
  [key: string]: Announcer
}

export class AnnounceHandler {
  public constructor() {}

  private _registeredAnnouncers: RegisteredAnnouncers = {}

  public init(): void {
    if (!configTree.announcer.enabled) {
      console.log('Announcer: Not enabled.')
      return
    }

    setInterval(async () => {
      console.log('Announcer: New round.')

      for (const channelID of configTree.announcer.randomMessageChannelIDs) {
        if (!probability(configTree.announcer.probability)) {
          continue
        }

        console.log(`Announcer: Hitting channel ${channelID}.`)

        let users: User[]

        try {
          users = getChannelUsers(channelID)
        } catch (ex) {
          console.warn(ex)
          continue
        }

        if (!users.length) {
          continue
        }

        const chosenOne = randomFrom(users)

        console.log(`Announcer: Hitting user ${chosenOne.id}.`)

        const usedAnnouncerName =
          randomFrom(Object.keys(this._registeredAnnouncers))

        const usedAnnouncer = this._registeredAnnouncers[usedAnnouncerName]

        console.log(`Announcer: Using announcer '${usedAnnouncerName}'.`)

        const message = await usedAnnouncer.execute(chosenOne)

        if (!message) {
          continue
        }

        client.postMessage(channelID, message)
      }
    }, configTree.announcer.interval)
  }

  public registerAnnouncer(announcer: Announcer): void {
    this._registeredAnnouncers[announcer.name] = announcer

    console.log(`Registered announcer '${announcer.name}'.`)
  }
}
